#include <QDir>
#include <QDateTime>
#include <QFile>
#include <QInputDialog>
#include <QMessageBox>
#include <QProcess>
#include <QDebug>
#include <QTextCodec>
#include <QResource>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "wraper_arecord.h"

MainWindow::MainWindow(QWidget *parent)    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    palette_red = new QPalette();
    palette_yellow = new QPalette();
    palette_green = new QPalette();
    palette_blue = new QPalette();
    palette_red->setColor(QPalette::Highlight, *(new QColor(0xBF, 0, 0)));
    palette_yellow->setColor(QPalette::Highlight, *(new QColor(0xBF, 0x7F, 0)));
    palette_green->setColor(QPalette::Highlight, *(new QColor(0, 0xBF, 0)));
    palette_blue->setColor(QPalette::Highlight, *(new QColor(0, 0, 0xBF)));
    ui->bar_volume->setFormat("Waiting");
    ui->bar_volume->setValue(100);
    ui->bar_volume->setPalette(*palette_blue);
    RecordingType = MainWindow::TYPE_WAVE;

/** FIXME: version **/
    setWindowTitle("KVoiceRecorder v0.7");

    wraper_arecord::init(this);
    QString temp_filename = "/tmp/KVoiceRecorder_";
    temp_filename.append(QDateTime::currentDateTime().toString("yyyyMMddhhmmss"));
    temp_filename.append(".raw");
    qDebug() << "Filnamn: " << temp_filename << "\n";
    wraper_arecord::set_filename(temp_filename);
    qDebug() << "Filnamn: " << wraper_arecord::filename << "\n";
    wraper_arecord::set_alas_settings();

    setRecordingQuality(1,22050);
    setRecordingQuality(2,16);

    if(!wraper_arecord::check_raw())
    {
        QMessageBox::warning(this, "Dependings missing", "Can't find arecord, please tell your administrator to install arecord\nin gentoo: emerge -a alsa-utils");
        exit(1);
    }

    testRecordingTypes();
    updateWindow();
    qDebug() << "Current State: " << current_state << "\n";
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::get_current_state()
{
    if(wraper_arecord::is_recording())
    {
        current_state = STATE_RECORDING;
    }
    else if(wraper_arecord::is_playing())
    {
        current_state = STATE_PLAYING;
    }
    else if(!QFile::exists(wraper_arecord::filename))
    {
        current_state = STATE_BLANK;
    }
    else if(saved_filename.length())
    {
        current_state = STATE_SAVED;
    }
    else
    {
        current_state = STATE_PAUSED;
    }
    return current_state;
}

void MainWindow::finishedRecording(int exitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << "exitCode: " << exitCode;
    qDebug() << "exitStatus: " << exitStatus;
    if(exitCode > 0)
    {
        ui->bar_volume->setFormat("Error, Recording failed");
        wraper_arecord::debug_process_error();
    }
    updateWindow();
}

void MainWindow::updateWindow()
{
    get_current_state();

    qDebug() << QDateTime::currentDateTime().toString("hh:mm:ss") << " - Curent State: " << current_state << "\n";

    /* button_control */
    if(current_state == STATE_PAUSED)
    {
        ui->button_control->setEnabled(false);
        ui->button_control->setText("Continue");
        ui->button_control->setIcon(QIcon(":img/record.png"));
    }
    else if(current_state == STATE_RECORDING)
    {
        ui->button_control->setEnabled(true);
        ui->button_control->setText("Stop");
        ui->button_control->setIcon(QIcon(":img/stop.png"));
    }
    else
    {
        ui->button_control->setEnabled(true);
        ui->button_control->setText("Record");
        ui->button_control->setIcon(QIcon(":img/record.png"));
    }

    /* button_reset */
    if(current_state == STATE_PAUSED)
    {
        ui->button_reset->setEnabled(true);
        ui->button_reset->setText("Clear");
        ui->button_reset->setIcon(QIcon(":img/new.png"));
    }
    else if(current_state == STATE_SAVED)
    {
        ui->button_reset->setEnabled(true);
        ui->button_reset->setText("Play");
        ui->button_reset->setIcon(QIcon(":img/play.png"));
    }
    else if(current_state == STATE_PLAYING)
    {
        ui->button_reset->setEnabled(true);
        ui->button_reset->setText("Stop");
        ui->button_reset->setIcon(QIcon(":img/stop.png"));
    }
    else
    {
        ui->button_reset->setEnabled(false);
        ui->button_reset->setText("Clear");
        ui->button_reset->setIcon(QIcon(":img/new.png"));
    }

    /* button_save */
    if(current_state == STATE_BLANK)
    {
        ui->button_save->setEnabled(false);
        //ui->button_save->setText("Save");
        //ui->button_save->setIcon(QIcon(":img/save.png"));
    }
    else
    {
        ui->button_save->setEnabled(true);
        //ui->button_save->setText("Save");
        //ui->button_save->setIcon(QIcon(":img/save.png"));
    }
}

void MainWindow::viewAbout()
{
    QString title;
    QString text;

/** FIXME: version **/
    title = "About KVoiceRecorder v0.7";
    text.append("\n");
    text.append("Releasedate: 2012-11-06\n");
    text.append("\n");
    text.append("Copyright 2009-2012\n");
    text.append("Thorbj\xf6rn Backlund, Sweden\n");
    text.append("Jonas Linhell, Sweden\n");
    text.append("Aron Agelii, Sweden\n");
    QMessageBox::information(this, title, text);
}

void MainWindow::viewStatus()
{
    QString title;
    QString text;

/** FIXME: version **/
    title = "Status KVoiceRecorder v0.7";
    text = "KVoiceRecorder v0.7\n";
    text.append("\n");
    text.append("Fileformat:\n");

    text.append("Raw: Arecord ");
    if(wraper_arecord::check_raw())
    {
        text.append("installed\n");
    }
    else
    {
        text.append("missing\n");
    }

    text.append("Wav: SoX ");
    if(wraper_arecord::check_wave())
    {
        text.append("installed\n");
    }
    else
    {
        text.append("missing\n");
    }

    text.append("MP3: LAME ");
    if(wraper_arecord::check_mp3())
    {
        text.append("installed\n");
    }
    else
    {
        text.append("missing\n");
    }

    text.append("OGG: oggenc ");
    if(wraper_arecord::check_ogg())
    {
        text.append("installed\n");
    }
    else
    {
        text.append("missing\n");
    }

    text.append("WMA: ffmpeg ");
    if(wraper_arecord::check_wma())
    {
        text.append("installed\n");
    }
    else
    {
        text.append("missing\n");
    }

    QMessageBox::information(this, title, text);
}

void MainWindow::button_control_clicked(void)
{
    switch(current_state)
    {
        case STATE_PLAYING:
            stopPlaying();
            deleteRecording();
            startRecording();
            break;
        case STATE_BLANK:
            startRecording();
            break;
        case STATE_SAVED:
            deleteRecording();
            startRecording();
            break;
        case STATE_RECORDING:
            stopRecording();
            break;
        case STATE_PAUSED:
            //countineuRecording();
            break;
    }
    updateWindow();
}
void MainWindow::button_reset_clicked(void)
{
    switch(current_state)
    {
        case STATE_RECORDING:
            stopRecording();
            deleteRecording();
            break;
        case STATE_BLANK:
        case STATE_PAUSED:
            deleteRecording();
            break;
        case STATE_SAVED:
            play();
            break;
        case STATE_PLAYING:
            stopPlaying();
            break;
    }

    updateWindow();
}
void MainWindow::button_save_clicked(void)
{
    switch(current_state)
    {
        case STATE_BLANK:
            updateWindow();
            return;
        case STATE_RECORDING:
            stopRecording();
            break;
        case STATE_PLAYING:
            stopPlaying();
            break;
        case STATE_PAUSED:
        case STATE_SAVED:
            break;
    }

    if(askForSaveName())
    {
        encodeRecording();
    }

    updateWindow();
}

/* Start/stop button */
void MainWindow::startRecording(void)
{
    if(current_state == STATE_BLANK)
    {
        if(wraper_arecord::record())
        {
            ui->bar_volume->setFormat("Recording");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_red);
        }
        else
        {
            QString text = "ERROR can't Start Recording :-(";
            ui->bar_volume->setFormat(text);
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_red);
        }
        updateWindow();
    }
}

void MainWindow::stopRecording(void)
{
    if(current_state == STATE_RECORDING)
    {
        if(wraper_arecord::pause())
        {
            ui->bar_volume->setFormat("Recorded");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_yellow);
        }
        else
        {
            ui->bar_volume->setFormat("ERROR can't Pause :-(");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_red);
        }
    }
    updateWindow();
}

/* Reset button */
void MainWindow::deleteRecording(void)
{
    QFile::remove(wraper_arecord::filename);
    ui->bar_volume->setFormat("Waiting");
    ui->bar_volume->setValue(100);
    ui->bar_volume->setPalette(*palette_blue);
    encoded_filename = "";
    saved_filename = "";

    updateWindow();
}

void MainWindow::play(void)
{
    if(current_state == STATE_SAVED)
    {
        wraper_arecord::play();
        ui->bar_volume->setFormat("Playing");
        ui->bar_volume->setValue(100);
        ui->bar_volume->setPalette(*palette_green);

    }
    updateWindow();
}

void MainWindow::stopPlaying(void)
{
    if(current_state == STATE_PLAYING)
    {
        wraper_arecord::stop();
        ui->bar_volume->setFormat("Waiting");
        ui->bar_volume->setValue(100);
        ui->bar_volume->setPalette(*palette_blue);
    }
    updateWindow();
}

bool MainWindow::askForSaveName(void)
{
    QString path;
    QString filename;
    QDir *filesystem;
    bool result;

    path = QDir::homePath();
    path.append("/");
    filesystem = new QDir(path);

    path.append("Dokument/");
    if(!filesystem->exists(path))
    {
qDebug() << "folder missing: " << path << "\n";

        if(!filesystem->mkdir(path))
        {
            ui->bar_volume->setFormat("Failed to create folder (1)");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_red);
            return false;
        }
    }

    path.append(QDateTime::currentDateTime().toString("yyyy-MM-dd/"));

    if(!filesystem->exists(path))
    {
        qDebug() << "folder missing: " << path << "\n";
        if(!filesystem->mkdir(path))
        {
            ui->bar_volume->setFormat("Failed to create folder (2)");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_red);
            return false;
        }
    }

    filename = QDateTime::currentDateTime().toString("hh_mm_ss");

    ui->bar_volume->setFormat("Waiting for filename");
    ui->bar_volume->repaint();

    filename = QInputDialog::getText(this,"Filename","Select filename to save file as",QLineEdit::Normal,filename,&result);

    if(result)
    {
        /* if not fileextension is 2-4 chars, and the default one (XXX6543XX) */
        if(!filename.right(6).left(4).contains("."))
        {
            switch(RecordingType)
            {
                case MainWindow::TYPE_MP3:
                    filename.append(".mp3");
                    break;
                case MainWindow::TYPE_OGG:
                    filename.append(".ogg");
                    break;
                case MainWindow::TYPE_WMA:
                    filename.append(".wma");
                    break;
                default:
                    filename.append(".wav");
                    break;
            }

        }

        saved_short_filename = filename;
        saved_filename = path;
        saved_filename.append(filename);
    }
    return result;
}

/* Save button */
void MainWindow::encodeRecording(void)
{
    QString raw_filename = wraper_arecord::filename;
    QString encoder_path;
    QStringList *arguments;
    QProcess *encoder_process;
    QFile *fil;

    /* Saving a recording while recording? */
    if(current_state == STATE_RECORDING)
    {
        /* stop recording by emulate button start/stop recording */
        startRecording();
    }

    fil = new QFile();

    if(!fil->exists(raw_filename))
    {
        ui->bar_volume->setFormat("Encoding Failed, wavefile missing");
        ui->bar_volume->setValue(100);
        ui->bar_volume->setPalette(*palette_red);
        return;
    }

    switch(RecordingType)
    {
        case MainWindow::TYPE_MP3:
            encoded_filename = raw_filename;
            encoded_filename.append(".mp3");

            if(fil->exists(encoded_filename))
            {
                fil->remove(encoded_filename);
                if(fil->exists(encoded_filename))
                {
                    ui->bar_volume->setFormat("Encoding Failed, can't remove old file");
                    ui->bar_volume->setValue(100);
                    ui->bar_volume->setPalette(*palette_red);
                    return;
                }
            }

            encoder_path = "lame";
            /* lame -r -s 8/11.025/22.05/44.1/48 --bitwidth 16/8 -m s/m */
            arguments = new QStringList();

            arguments->append("-r");
            arguments->append("-s");
            if(wraper_arecord::hertz == 44100)
            {
                arguments->append("44.1");
            }
            else if(wraper_arecord::hertz == 11025)
            {
                arguments->append("11.025");
            }
            else
            {
                arguments->append("22.05");
            }
            arguments->append("--bitwidth");
            if(wraper_arecord::deep == 8)
            {
                arguments->append("8");
            }
            else
            {
                arguments->append("16");
            }

            arguments->append("-m");

            if(wraper_arecord::stereo)
            {
                arguments->append("s");
            }
            else
            {
                arguments->append("m");
            }

            arguments->append(raw_filename);
            arguments->append(encoded_filename);
            ui->bar_volume->setFormat("Encoding MP3");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_green);
            ui->bar_volume->repaint();
            encoder_process = new QProcess();
            encoder_process->start(encoder_path, *arguments);
            encoder_process->waitForFinished();
            if(fil->exists(encoded_filename))
            {
                encodeRecordingComplete();
                return;
            }
            else
            {
                ui->bar_volume->setFormat("Encoding Failed, can't create file");
                ui->bar_volume->setValue(100);
                ui->bar_volume->setPalette(*palette_red);
                return;
            }
            break;

        case MainWindow::TYPE_OGG:
            encoded_filename = raw_filename;
            encoded_filename.append(".ogg");

            if(fil->exists(encoded_filename))
            {
                fil->remove(encoded_filename);
                if(fil->exists(encoded_filename))
                {
                    ui->bar_volume->setFormat("Encoding Failed, can't remove old file");
                    ui->bar_volume->setValue(100);
                    ui->bar_volume->setPalette(*palette_red);
                    return;
                }
            }

            encoder_path = "oggenc";
            arguments = new QStringList();

            if(wraper_arecord::hertz == 44100)
            {
                arguments->append("--raw-rate=44100");
            }
            else if(wraper_arecord::hertz == 11025)
            {
                arguments->append("--raw-rate=11025");
            }
            else
            {
                arguments->append("--raw-rate=22050");
            }
            if(wraper_arecord::deep == 8)
            {
                arguments->append("--raw-bits=8");
            }
            else
            {
                arguments->append("--raw-bits=16");
            }
            if(wraper_arecord::stereo)
            {
                arguments->append("--raw-chan=2");
            }
            else
            {
                arguments->append("--raw-chan=1");
            }
            arguments->append("--raw-chan=1");
            arguments->append("--quality");
            arguments->append("2");
            arguments->append("-o");
            arguments->append(encoded_filename);
            arguments->append(raw_filename);
            ui->bar_volume->setFormat("Encoding OGG");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_green);
            ui->bar_volume->repaint();
            encoder_process = new QProcess();
            encoder_process->start(encoder_path, *arguments);
            encoder_process->waitForFinished();
            if(fil->exists(encoded_filename))
            {
                encodeRecordingComplete();
                return;
            }
            else
            {
                ui->bar_volume->setFormat("Encoding Failed, can't create file");
                ui->bar_volume->setValue(100);
                ui->bar_volume->setPalette(*palette_red);
                return;
            }
            break;
        case MainWindow::TYPE_WMA:
            encoded_filename = raw_filename;
            encoded_filename.append(".wma");

            if(fil->exists(encoded_filename))
            {
                fil->remove(encoded_filename);
                if(fil->exists(encoded_filename))
                {
                    ui->bar_volume->setFormat("Encoding Failed, can't remove old file");
                    ui->bar_volume->setValue(100);
                    ui->bar_volume->setPalette(*palette_red);
                    return;
                }
            }

            encoder_path = "ffmpeg";
            arguments = new QStringList();
            arguments->append("-i");
            arguments->append(raw_filename);
            arguments->append("-acodec");
            arguments->append("wmav2");
            arguments->append("-ab");
            arguments->append("131072");
            arguments->append(encoded_filename);
            ui->bar_volume->setFormat("Encoding WMA");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_green);
            ui->bar_volume->repaint();
            encoder_process = new QProcess();
            encoder_process->start(encoder_path, *arguments);
            encoder_process->waitForFinished();
            if(fil->exists(encoded_filename))
            {
                encodeRecordingComplete();
                return;
            }
            else
            {
                ui->bar_volume->setFormat("Encoding Failed, can't create file");
                ui->bar_volume->setValue(100);
                ui->bar_volume->setPalette(*palette_red);
                return;
            }
            break;
        /* WAVE */
        default:
            encoded_filename = raw_filename;
            encoded_filename.append(".wav");
            if(fil->exists(encoded_filename))
            {
                fil->remove(encoded_filename);
                if(fil->exists(encoded_filename))
                {
                    ui->bar_volume->setFormat("Encoding Failed, can't remove old file");
                    ui->bar_volume->setValue(100);
                    ui->bar_volume->setPalette(*palette_red);
                    return;
                }
            }

            if(wraper_arecord::check_wave())
            {
                encoder_path = "sox";
                arguments = new QStringList();
                arguments->append("-i");
                arguments->append(raw_filename);
                arguments->append("-acodec");
                arguments->append("wmav2");
                arguments->append("-ab");
                arguments->append("131072");
                arguments->append(encoded_filename);
                ui->bar_volume->setFormat("Encoding WAV");
                ui->bar_volume->setValue(100);
                ui->bar_volume->setPalette(*palette_green);
                ui->bar_volume->repaint();
                encoder_process = new QProcess();
                encoder_process->start(encoder_path, *arguments);
                encoder_process->waitForFinished();
                if(fil->exists(encoded_filename))
                {
                    encodeRecordingComplete();
                    return;
                }
                else
                {
                    ui->bar_volume->setFormat("Encoding Failed, can't create file");
                    ui->bar_volume->setValue(100);
                    ui->bar_volume->setPalette(*palette_red);
                    return;
                }
            }
            else
            {
                QFile::copy(raw_filename, encoded_filename);
                ui->bar_volume->setFormat("Encoding");
                ui->bar_volume->setValue(100);
                ui->bar_volume->setPalette(*palette_green);
                ui->bar_volume->repaint();
                encodeRecordingComplete();
                break;
            }
    }
}

/* signal trigered encodeRecordingComplete */
void MainWindow::saveRecording(void)
{
    QString path;
    QString filename;
    QString status_text;

    ui->bar_volume->setFormat("Saving");
    ui->bar_volume->setValue(100);
    ui->bar_volume->setPalette(*palette_green);

    QFile *sound_file;

    sound_file = new QFile(encoded_filename);

    if(sound_file->exists(saved_filename))
    {
        if(QMessageBox::Yes == QMessageBox::warning(this,"Overwrite file?","The file alredy exist, overwrite it?",QMessageBox::Yes,QMessageBox::No))
        {
            sound_file->remove(saved_filename);
        }
        else
        {
            ui->bar_volume->setFormat("Not saved, file existed");
            ui->bar_volume->setValue(100);
            ui->bar_volume->setPalette(*palette_red);
            return;
        }
    }

    if(!sound_file->rename(saved_filename))
    {
        ui->bar_volume->setFormat("Failed to move file");
        ui->bar_volume->setValue(100);
        ui->bar_volume->setPalette(*palette_red);
        return;
    }

    status_text = "Saved as ";
    status_text.append(saved_short_filename);
    ui->bar_volume->setFormat(status_text);
    ui->bar_volume->setValue(100);
    ui->bar_volume->setPalette(*palette_green);

    updateWindow();
    saveRecordingComplete();
}

void MainWindow::testRecordingTypes(void)
{
    if(0) //wraper_arecord::check_wma())
    {
        RecordingType = MainWindow::TYPE_WMA;
        ui->action_wma->setEnabled(true);
    }

    if(wraper_arecord::check_ogg())
    {
        RecordingType = MainWindow::TYPE_OGG;
        ui->action_ogg->setEnabled(true);
    }

    if(wraper_arecord::check_mp3())
    {
        RecordingType = MainWindow::TYPE_MP3;
        ui->action_mp3->setEnabled(true);
    }

    MainWindow::setRecordingType(RecordingType);
}

/* menu actions */
void MainWindow::setRecordingType_mp3(void) {MainWindow::setRecordingType(MainWindow::TYPE_MP3);}
void MainWindow::setRecordingType_ogg(void) {MainWindow::setRecordingType(MainWindow::TYPE_OGG);}
void MainWindow::setRecordingType_wma(void) {MainWindow::setRecordingType(MainWindow::TYPE_WMA);}
void MainWindow::setRecordingType_wave(void) {MainWindow::setRecordingType(MainWindow::TYPE_WAVE);}

void MainWindow::setRecordingType(int new_type)
{
    /* prevent infinitive loops */
    static bool working = false;
    if(!working)
    {
        working = true;
        RecordingType = new_type;
        ui->action_mp3->setChecked(false);
        ui->action_ogg->setChecked(false);
        ui->action_wma->setChecked(false);
        ui->action_wave->setChecked(false);

        switch(RecordingType)
        {
            case MainWindow::TYPE_MP3:
                ui->action_mp3->setChecked(true);
                break;
            case MainWindow::TYPE_OGG:
                ui->action_ogg->setChecked(true);
                break;
            case MainWindow::TYPE_WMA:
                ui->action_wma->setChecked(true);
                break;
            case MainWindow::TYPE_WAVE:
                ui->action_wave->setChecked(true);
                break;
            default:
                ui->action_wave->setChecked(true);
                RecordingType = MainWindow::TYPE_WAVE;
                break;
        }
        working = false;
    }
}

void MainWindow::setRecordingQuality44(void) { setRecordingQuality(1,44100); }
void MainWindow::setRecordingQuality22(void) { setRecordingQuality(1,22050); }
void MainWindow::setRecordingQuality11(void) { setRecordingQuality(1,11025); }
void MainWindow::setRecordingQuality16(void) { setRecordingQuality(2,16); }
void MainWindow::setRecordingQuality8(void) { setRecordingQuality(2,8); }

void MainWindow::setRecordingQuality(int type, int value)
{
    static bool working = false;
    if(!working)
    {
        working = true;
        if(type == 1)
        {
            if(value == 44100)
            {
                wraper_arecord::hertz = 44100;
            }
            else if(value == 11025)
            {
                wraper_arecord::hertz = 11025;
            }
            else
            {
                wraper_arecord::hertz = 22050;
            }
        }
        if(type == 2)
        {
            if(value == 8)
            {
                wraper_arecord::deep = 8;
            }
            else
            {
                wraper_arecord::deep = 16;
            }
        }
        ui->action_11khz_8bit->setChecked( (wraper_arecord::deep == 8) && (wraper_arecord::hertz == 11025) );
        ui->action_22khz_8bit->setChecked( (wraper_arecord::deep == 8) && (wraper_arecord::hertz == 22050) );
        ui->action_44khz_8bit->setChecked( (wraper_arecord::deep == 8) && (wraper_arecord::hertz == 44100) );
        ui->action_22khz_16bit->setChecked( (wraper_arecord::deep == 16) && (wraper_arecord::hertz == 22050) );
        ui->action_44khz_16bit->setChecked( (wraper_arecord::deep == 16) && (wraper_arecord::hertz == 44100) );
        working = false;
    }
}
