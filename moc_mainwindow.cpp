/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Wed Oct 3 10:39:34 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      36,   11,   11,   11, 0x05,
      62,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      88,   11,   11,   11, 0x0a,
     113,   11,   11,   11, 0x0a,
     136,   11,   11,   11, 0x0a,
     158,   11,   11,   11, 0x0a,
     175,   11,   11,   11, 0x0a,
     191,   11,   11,   11, 0x0a,
     198,   11,   11,   11, 0x0a,
     212,   11,   11,   11, 0x0a,
     230,   11,   11,   11, 0x0a,
     248,   11,   11,   11, 0x0a,
     264,   11,   11,   11, 0x0a,
     287,   11,   11,   11, 0x0a,
     310,   11,   11,   11, 0x0a,
     333,   11,   11,   11, 0x0a,
     357,   11,   11,   11, 0x0a,
     381,   11,   11,   11, 0x0a,
     405,   11,   11,   11, 0x0a,
     429,   11,   11,   11, 0x0a,
     453,   11,   11,   11, 0x0a,
     476,   11,   11,   11, 0x0a,
     488,   11,   11,   11, 0x0a,
     501,   11,   11,   11, 0x0a,
     536,  516,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0saveRecordingComplete()\0"
    "encodeRecordingComplete()\0"
    "deleteRecordingComplete()\0"
    "button_control_clicked()\0"
    "button_reset_clicked()\0button_save_clicked()\0"
    "startRecording()\0stopRecording()\0"
    "play()\0stopPlaying()\0encodeRecording()\0"
    "deleteRecording()\0saveRecording()\0"
    "setRecordingType_mp3()\0setRecordingType_ogg()\0"
    "setRecordingType_wma()\0setRecordingType_wave()\0"
    "setRecordingQuality44()\0setRecordingQuality22()\0"
    "setRecordingQuality11()\0setRecordingQuality16()\0"
    "setRecordingQuality8()\0viewAbout()\0"
    "viewStatus()\0updateWindow()\0"
    "exitCode,exitStatus\0"
    "finishedRecording(int,QProcess::ExitStatus)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->saveRecordingComplete(); break;
        case 1: _t->encodeRecordingComplete(); break;
        case 2: _t->deleteRecordingComplete(); break;
        case 3: _t->button_control_clicked(); break;
        case 4: _t->button_reset_clicked(); break;
        case 5: _t->button_save_clicked(); break;
        case 6: _t->startRecording(); break;
        case 7: _t->stopRecording(); break;
        case 8: _t->play(); break;
        case 9: _t->stopPlaying(); break;
        case 10: _t->encodeRecording(); break;
        case 11: _t->deleteRecording(); break;
        case 12: _t->saveRecording(); break;
        case 13: _t->setRecordingType_mp3(); break;
        case 14: _t->setRecordingType_ogg(); break;
        case 15: _t->setRecordingType_wma(); break;
        case 16: _t->setRecordingType_wave(); break;
        case 17: _t->setRecordingQuality44(); break;
        case 18: _t->setRecordingQuality22(); break;
        case 19: _t->setRecordingQuality11(); break;
        case 20: _t->setRecordingQuality16(); break;
        case 21: _t->setRecordingQuality8(); break;
        case 22: _t->viewAbout(); break;
        case 23: _t->viewStatus(); break;
        case 24: _t->updateWindow(); break;
        case 25: _t->finishedRecording((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::saveRecordingComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void MainWindow::encodeRecordingComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void MainWindow::deleteRecordingComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
