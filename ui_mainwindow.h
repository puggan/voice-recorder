/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed Oct 3 10:39:32 2012
**      by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_new;
    QAction *action_save;
    QAction *action_exit;
    QAction *action_about;
    QAction *action_wave;
    QAction *action_mp3;
    QAction *action_ogg;
    QAction *action_wma;
    QAction *action_44khz_16bit;
    QAction *action_22khz_16bit;
    QAction *action_44khz_8bit;
    QAction *action_22khz_8bit;
    QAction *action_11khz_8bit;
    QAction *action_status;
    QWidget *layout_main;
    QGridLayout *gridLayout;
    QProgressBar *bar_volume;
    QPushButton *button_control;
    QPushButton *button_reset;
    QPushButton *button_save;
    QMenuBar *menubar;
    QMenu *menu_file;
    QMenu *menu_settings;
    QMenu *menu_type;
    QMenu *menu_quality;
    QMenu *menu_help;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(280, 109);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/img/logo_96.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setIconSize(QSize(96, 96));
        action_new = new QAction(MainWindow);
        action_new->setObjectName(QString::fromUtf8("action_new"));
        action_new->setEnabled(false);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/img/new.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_new->setIcon(icon1);
        action_save = new QAction(MainWindow);
        action_save->setObjectName(QString::fromUtf8("action_save"));
        action_save->setEnabled(false);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/img/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_save->setIcon(icon2);
        action_exit = new QAction(MainWindow);
        action_exit->setObjectName(QString::fromUtf8("action_exit"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/img/stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_exit->setIcon(icon3);
        action_about = new QAction(MainWindow);
        action_about->setObjectName(QString::fromUtf8("action_about"));
        action_about->setEnabled(true);
        action_wave = new QAction(MainWindow);
        action_wave->setObjectName(QString::fromUtf8("action_wave"));
        action_wave->setCheckable(true);
        action_wave->setChecked(false);
        action_wave->setEnabled(true);
        action_mp3 = new QAction(MainWindow);
        action_mp3->setObjectName(QString::fromUtf8("action_mp3"));
        action_mp3->setCheckable(true);
        action_mp3->setChecked(false);
        action_mp3->setEnabled(false);
        action_ogg = new QAction(MainWindow);
        action_ogg->setObjectName(QString::fromUtf8("action_ogg"));
        action_ogg->setCheckable(true);
        action_ogg->setEnabled(false);
        action_wma = new QAction(MainWindow);
        action_wma->setObjectName(QString::fromUtf8("action_wma"));
        action_wma->setCheckable(true);
        action_wma->setEnabled(false);
        action_44khz_16bit = new QAction(MainWindow);
        action_44khz_16bit->setObjectName(QString::fromUtf8("action_44khz_16bit"));
        action_44khz_16bit->setCheckable(true);
        action_44khz_16bit->setChecked(false);
        action_44khz_16bit->setEnabled(true);
        action_22khz_16bit = new QAction(MainWindow);
        action_22khz_16bit->setObjectName(QString::fromUtf8("action_22khz_16bit"));
        action_22khz_16bit->setCheckable(true);
        action_22khz_16bit->setChecked(true);
        action_22khz_16bit->setEnabled(true);
        action_44khz_8bit = new QAction(MainWindow);
        action_44khz_8bit->setObjectName(QString::fromUtf8("action_44khz_8bit"));
        action_44khz_8bit->setCheckable(true);
        action_44khz_8bit->setEnabled(true);
        action_22khz_8bit = new QAction(MainWindow);
        action_22khz_8bit->setObjectName(QString::fromUtf8("action_22khz_8bit"));
        action_22khz_8bit->setCheckable(true);
        action_22khz_8bit->setEnabled(true);
        action_11khz_8bit = new QAction(MainWindow);
        action_11khz_8bit->setObjectName(QString::fromUtf8("action_11khz_8bit"));
        action_11khz_8bit->setCheckable(true);
        action_11khz_8bit->setEnabled(true);
        action_status = new QAction(MainWindow);
        action_status->setObjectName(QString::fromUtf8("action_status"));
        layout_main = new QWidget(MainWindow);
        layout_main->setObjectName(QString::fromUtf8("layout_main"));
        gridLayout = new QGridLayout(layout_main);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        bar_volume = new QProgressBar(layout_main);
        bar_volume->setObjectName(QString::fromUtf8("bar_volume"));
        QFont font;
        font.setBold(true);
        font.setItalic(false);
        font.setUnderline(false);
        font.setWeight(75);
        bar_volume->setFont(font);
        bar_volume->setMaximum(100);
        bar_volume->setValue(100);
        bar_volume->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(bar_volume, 0, 0, 1, 3);

        button_control = new QPushButton(layout_main);
        button_control->setObjectName(QString::fromUtf8("button_control"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/img/record.png"), QSize(), QIcon::Normal, QIcon::Off);
        button_control->setIcon(icon4);
        button_control->setDefault(false);
        button_control->setFlat(false);

        gridLayout->addWidget(button_control, 1, 0, 1, 1);

        button_reset = new QPushButton(layout_main);
        button_reset->setObjectName(QString::fromUtf8("button_reset"));
        button_reset->setEnabled(false);
        button_reset->setIcon(icon1);

        gridLayout->addWidget(button_reset, 1, 1, 1, 1);

        button_save = new QPushButton(layout_main);
        button_save->setObjectName(QString::fromUtf8("button_save"));
        button_save->setEnabled(false);
        button_save->setIcon(icon2);

        gridLayout->addWidget(button_save, 1, 2, 1, 1);

        MainWindow->setCentralWidget(layout_main);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 280, 21));
        menu_file = new QMenu(menubar);
        menu_file->setObjectName(QString::fromUtf8("menu_file"));
        menu_settings = new QMenu(menubar);
        menu_settings->setObjectName(QString::fromUtf8("menu_settings"));
        menu_type = new QMenu(menu_settings);
        menu_type->setObjectName(QString::fromUtf8("menu_type"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/img/type.png"), QSize(), QIcon::Normal, QIcon::Off);
        menu_type->setIcon(icon5);
        menu_quality = new QMenu(menu_settings);
        menu_quality->setObjectName(QString::fromUtf8("menu_quality"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/img/quality.png"), QSize(), QIcon::Normal, QIcon::Off);
        menu_quality->setIcon(icon6);
        menu_help = new QMenu(menubar);
        menu_help->setObjectName(QString::fromUtf8("menu_help"));
        MainWindow->setMenuBar(menubar);

        menubar->addAction(menu_file->menuAction());
        menubar->addAction(menu_settings->menuAction());
        menubar->addAction(menu_help->menuAction());
        menu_file->addAction(action_new);
        menu_file->addAction(action_save);
        menu_file->addAction(action_exit);
        menu_settings->addAction(menu_type->menuAction());
        menu_settings->addAction(menu_quality->menuAction());
        menu_type->addAction(action_wave);
        menu_type->addSeparator();
        menu_type->addAction(action_mp3);
        menu_type->addAction(action_ogg);
        menu_type->addAction(action_wma);
        menu_quality->addAction(action_44khz_16bit);
        menu_quality->addAction(action_22khz_16bit);
        menu_quality->addSeparator();
        menu_quality->addAction(action_44khz_8bit);
        menu_quality->addAction(action_22khz_8bit);
        menu_quality->addAction(action_11khz_8bit);
        menu_help->addAction(action_about);
        menu_help->addAction(action_status);

        retranslateUi(MainWindow);
        QObject::connect(action_exit, SIGNAL(triggered()), MainWindow, SLOT(close()));
        QObject::connect(button_control, SIGNAL(clicked()), MainWindow, SLOT(button_control_clicked()));
        QObject::connect(button_reset, SIGNAL(clicked()), MainWindow, SLOT(button_reset_clicked()));
        QObject::connect(button_save, SIGNAL(clicked()), MainWindow, SLOT(button_save_clicked()));
        QObject::connect(action_11khz_8bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality11()));
        QObject::connect(action_22khz_8bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality22()));
        QObject::connect(action_44khz_8bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality44()));
        QObject::connect(action_22khz_16bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality22()));
        QObject::connect(action_44khz_16bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality44()));
        QObject::connect(action_mp3, SIGNAL(toggled(bool)), MainWindow, SLOT(setRecordingType_mp3()));
        QObject::connect(action_ogg, SIGNAL(toggled(bool)), MainWindow, SLOT(setRecordingType_ogg()));
        QObject::connect(action_wma, SIGNAL(toggled(bool)), MainWindow, SLOT(setRecordingType_wma()));
        QObject::connect(action_wave, SIGNAL(toggled(bool)), MainWindow, SLOT(setRecordingType_wave()));
        QObject::connect(action_new, SIGNAL(triggered()), MainWindow, SLOT(deleteRecording()));
        QObject::connect(action_save, SIGNAL(triggered()), MainWindow, SLOT(encodeRecording()));
        QObject::connect(MainWindow, SIGNAL(encodeRecordingComplete()), MainWindow, SLOT(saveRecording()));
        QObject::connect(action_about, SIGNAL(triggered()), MainWindow, SLOT(viewAbout()));
        QObject::connect(action_status, SIGNAL(triggered()), MainWindow, SLOT(viewStatus()));
        QObject::connect(action_11khz_8bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality8()));
        QObject::connect(action_22khz_16bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality16()));
        QObject::connect(action_22khz_8bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality8()));
        QObject::connect(action_44khz_16bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality16()));
        QObject::connect(action_44khz_8bit, SIGNAL(triggered()), MainWindow, SLOT(setRecordingQuality8()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "KVoiceRecorder (loading)", 0, QApplication::UnicodeUTF8));
        action_new->setText(QApplication::translate("MainWindow", "&New", 0, QApplication::UnicodeUTF8));
        action_new->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", 0, QApplication::UnicodeUTF8));
        action_save->setText(QApplication::translate("MainWindow", "&Save As...", 0, QApplication::UnicodeUTF8));
        action_save->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0, QApplication::UnicodeUTF8));
        action_exit->setText(QApplication::translate("MainWindow", "E&xit", 0, QApplication::UnicodeUTF8));
        action_exit->setShortcut(QApplication::translate("MainWindow", "Ctrl+X", 0, QApplication::UnicodeUTF8));
        action_about->setText(QApplication::translate("MainWindow", "&About", 0, QApplication::UnicodeUTF8));
        action_wave->setText(QApplication::translate("MainWindow", "WAV", 0, QApplication::UnicodeUTF8));
        action_mp3->setText(QApplication::translate("MainWindow", "MP3", 0, QApplication::UnicodeUTF8));
        action_ogg->setText(QApplication::translate("MainWindow", "OGG", 0, QApplication::UnicodeUTF8));
        action_wma->setText(QApplication::translate("MainWindow", "WMA", 0, QApplication::UnicodeUTF8));
        action_44khz_16bit->setText(QApplication::translate("MainWindow", "44Khz, 16bit", 0, QApplication::UnicodeUTF8));
        action_22khz_16bit->setText(QApplication::translate("MainWindow", "22Khz, 16bit", 0, QApplication::UnicodeUTF8));
        action_44khz_8bit->setText(QApplication::translate("MainWindow", "44Khz, 8bit", 0, QApplication::UnicodeUTF8));
        action_22khz_8bit->setText(QApplication::translate("MainWindow", "22Khz, 8bit", 0, QApplication::UnicodeUTF8));
        action_11khz_8bit->setText(QApplication::translate("MainWindow", "11Khz, 8bit", 0, QApplication::UnicodeUTF8));
        action_status->setText(QApplication::translate("MainWindow", "Status", 0, QApplication::UnicodeUTF8));
        bar_volume->setFormat(QApplication::translate("MainWindow", "Waiting", 0, QApplication::UnicodeUTF8));
        button_control->setText(QApplication::translate("MainWindow", "New", 0, QApplication::UnicodeUTF8));
        button_reset->setText(QApplication::translate("MainWindow", "Clear", 0, QApplication::UnicodeUTF8));
        button_save->setText(QApplication::translate("MainWindow", "Save", 0, QApplication::UnicodeUTF8));
        menu_file->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menu_settings->setTitle(QApplication::translate("MainWindow", "Settings", 0, QApplication::UnicodeUTF8));
        menu_type->setTitle(QApplication::translate("MainWindow", "&Type", 0, QApplication::UnicodeUTF8));
        menu_quality->setTitle(QApplication::translate("MainWindow", "&Quality", 0, QApplication::UnicodeUTF8));
        menu_help->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
