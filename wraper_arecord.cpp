#include <QDebug>
#include <QProcess>
#include <qstring.h>
#include "wraper_arecord.h"
#include "ui_mainwindow.h"

namespace wraper_arecord
{
    QStringList *arguments;
    QProcess *process;
    QProcess *play_process;
    QObject *parent;

    QString exec_filename;
    QString play_exec_filename;
    QString filename;
    QString device;
    QString format;
    QString rate;
    int hertz;
    int deep;

    //bool nonblocking;
    bool jack;
    bool stereo;

    void init(QObject *the_parent)
    {
        set_exec_filename("arecord");
        //set_exec_filename("/usr/bin/arecord");
        set_filename("/tmp/ifw_recorder.wav");
        hertz = 22050;
        deep = 16;
        parent = the_parent;
        process = new QProcess(parent);
        QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), the_parent, SLOT(finishedRecording(int, QProcess::ExitStatus)));
//        QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), the_parent, SLOT(updateWindow()));
//        QObject::connect(process, SIGNAL(error(QProcess::ProcessError)), the_parent, SLOT(updateWindow()));

        play_exec_filename = "aplay";
        play_process = new QProcess(parent);
        QObject::connect(play_process, SIGNAL(finished(int, QProcess::ExitStatus)), the_parent, SLOT(updateWindow()));
        QObject::connect(play_process, SIGNAL(error(QProcess::ProcessError)), the_parent, SLOT(updateWindow()));

        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        env.insert("LANG", "");
        process->setProcessEnvironment(env);
        play_process->setProcessEnvironment(env);

        jack = check_jack();
        stereo = false;
    }

    void debug_process_error(void)
    {
        QByteArray error = process->readAllStandardError();
        qDebug() << error;
//        if(stereo == false)
//        {
//            if(error.contains("Channels count non available"))
//            {
//                qDebug() << "Switching to Stereo";
//                stereo = true;
//                record();
//            }
//        }
    }

    void update_parameters()
    {
            reset_parameters();
            add_parameter("--nonblock");

            if(stereo)
            {
                add_parameter("--channels=2");
            }
            else
            {
                add_parameter("--channels=1");
            }

            if(jack)
            {
                add_parameter("--device=plug:jack2");
            }
            else
            {
                add_parameter("--device=plug:default");
            }

            if(hertz == 44100)
            {
                add_parameter("--rate=44100");
            }
            else if(hertz == 11025)
            {
                add_parameter("--rate=11025");
            }
            else
            {
                add_parameter("--rate=22050");
            }
            if(deep == 8)
            {
                add_parameter("--format=U8");
            }
            else
            {
                add_parameter("--format=S16_LE");
            }
            add_parameter(filename); // add savefile
    }

    void set_exec_filename(QString new_exec_filename)
    {
            exec_filename =  new_exec_filename;
    }

    void set_filename(QString new_filename)
    {
            filename = new_filename;
            update_parameters();
    }

    void reset_parameters()
    {
            arguments = new QStringList();
    }

    void add_parameter(QString new_parameter)
    {
            arguments->append(new_parameter);
    }

    bool is_recording()
    {
            if(process->state())
            {
                    return TRUE;
            }
            else
            {
                    return FALSE;
            }
    }

    bool is_playing(void)
    {
            if(play_process->state())
            {
                    return TRUE;
            }
            else
            {
                    return FALSE;
            }
    }

    bool record()
    {
            if(is_recording())
            {
                    return FALSE;
            }
            else
            {
                    update_parameters();
                    process->start(exec_filename, *arguments);
                    process->waitForStarted();
                    return is_recording();
            }
    }

    bool pause()
    {
            if(is_recording())
            {
                    process->close();
                    return !is_recording();
            }
            else
            {
                    return FALSE;
            }
    }

    bool play()
    {
            if(is_playing())
            {
                    return FALSE;
            }
            else
            {
                    QStringList *play_arguments = new QStringList();
                    play_arguments->append(filename);
                    play_process->start(play_exec_filename, *play_arguments);
                    play_process->waitForStarted();
                    return is_playing();
            }
    }

    bool stop()
    {
            if(is_playing())
            {
                    play_process->close();
                    return !is_playing();
            }
            else
            {
                    return FALSE;
            }
    }

    bool set_alas_settings(void)
    {
	/*
	amixer sget "Mic Boost (+20dB)"
	amixer sset "Mic Boost (+20dB)" on
	amixer sset "Mic Boost (+20dB)" off

        amixer sget "Mic"
        amixer sset "Mic Boost (+20dB)" on

        amixer sget "Mic"
	amixer sset "Mic" cap

	amixer sget "Mic Select"

	amixer sget "Capture"
	amixer sset "Capture" 75%
	*/
        return FALSE;
    }

    bool check_raw(void)
    {
        /* arecord --version */
        QProcess *p = new QProcess(parent);
        QStringList *arg = new QStringList();
        arg->append("--version");
        p->start("arecord", *arg);
        if(p->waitForFinished())
        {
            //qDebug() << "check raw:\n" << ((new QString(p->readAllStandardOutput()))->trimmed()) << "\n\n";
            return TRUE;
        }
        else
        {
            //qDebug() << "check raw:\n   arecord not found\n\n";
            return FALSE;
        }
    }

    bool check_wave(void)
    {
        return FALSE;

        /* arecord --version */
        QProcess *p = new QProcess(parent);
        QStringList *arg = new QStringList();
        arg->append("--version");
        p->start("sox", *arg);
        if(p->waitForFinished())
        {
            //qDebug() << "check wave:\n" << ((new QString(p->readAllStandardOutput()))->trimmed()) << "\n\n";
            return TRUE;
        }
        else
        {
            //qDebug() << "check wave:\n   arecord not found\n\n";
            return FALSE;
        }
    }

    bool check_mp3(void)
    {
        /* lame --help (first row) */
        QProcess *p = new QProcess(parent);
        QStringList *arg = new QStringList();
        arg->append("--help");
        p->start("lame", *arg);
        if(p->waitForFinished())
        {
            //qDebug() << "check mp3:\n" << ((new QString(p->readAllStandardOutput()))->trimmed()) << "\n\n";
            return TRUE;
        }
        else
        {
            //qDebug() << "check mp3:\n   lame not found\n\n";
            return FALSE;
        }
    }

    bool check_ogg(void)
    {
        /* oggenc -v */
        QProcess *p = new QProcess(parent);
        QStringList *arg = new QStringList();
        arg->append("-v");
        p->start("oggenc", *arg);
        if(p->waitForFinished())
        {
            //qDebug() << "check ogg:\n" << ((new QString(p->readAllStandardOutput()))->trimmed()) << "\n\n";
            return TRUE;
        }
        else
        {
            //qDebug() << "check ogg:\n   oggenc not found\n\n";
            return FALSE;
        }
    }

    bool check_wma(void)
    {
        /* ffmpeg (first row) */
        QProcess *p = new QProcess(parent);
        QStringList *arg = new QStringList();
        p->start("ffmpeg", *arg);
        if(p->waitForFinished())
        {
            //qDebug() << "check wma:\n" << ((new QString(p->readAllStandardError()))->trimmed()) << "\n\n";
            return TRUE;
        }
        else
        {
            //qDebug() << "check wma:\n   ffmpeg not found\n\n";
            return FALSE;
        }
    }

    bool check_jack(void)
    {
        QProcess* bash = new QProcess(parent);
        QStringList* bash_arguments = new QStringList();
        bash_arguments->append("-c");
        bash_arguments->append("arecord -L | egrep '^[^\t :][^:]*$'");
        bash->start("bash", *bash_arguments);
        bash->waitForFinished();
        QByteArray bash_result = bash->readAllStandardOutput();
        qDebug() << "Avaible alsa devises" << bash_result;
        if(bash_result.contains("jack2"))
        {
            qDebug() << "switching to device: jack2";
        }
        return bash_result.contains("jack2");
    }
}
