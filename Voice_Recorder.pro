# -------------------------------------------------
# Project created by QtCreator 2009-11-13T23:07:06
# -------------------------------------------------
TARGET = Voice_Recorder
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    wraper_arecord.cpp
HEADERS += mainwindow.h \
    wraper_arecord.h
FORMS += mainwindow.ui
RESOURCES += images.qrc
