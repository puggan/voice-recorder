#include <QString>

namespace wraper_arecord
{
    extern QString filename;
    extern int hertz;
    extern int deep;
    extern bool stereo;

    void init(QObject *the_parent);
    void debug_process_error(void);
    void update_parameters(void);
    void set_exec_filename(QString new_exec_filename);
    void set_filename(QString new_filename);
    void reset_parameters(void);
    void add_parameter(QString new_parameter);
    bool is_recording(void);
    bool is_playing(void);
    bool record(void);
    bool pause(void);
    bool play(void);
    bool stop(void);
    bool set_alas_settings(void);

    bool check_raw(void);
    bool check_wave(void);
    bool check_mp3(void);
    bool check_ogg(void);
    bool check_wma(void);
    bool check_jack(void);
}
