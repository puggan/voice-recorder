#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QProcess>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void testRecordingTypes(void);
    void setRecordingType(int type);
    void setRecordingQuality(int type, int value);
    int get_current_state(void);
    bool askForSaveName(void);
    static const int TYPE_WAVE = 0;
    static const int TYPE_MP3 = 1;
    static const int TYPE_OGG = 2;
    static const int TYPE_WMA = 3;
    static const int STATE_BLANK = 0;
    static const int STATE_RECORDING = 1;
    static const int STATE_PAUSED = 2;
    static const int STATE_SAVED = 3;
    static const int STATE_PLAYING = 4;

private:
    int RecordingType;
    int current_state;
    Ui::MainWindow *ui;
    QPalette *palette_red;
    QPalette *palette_yellow;
    QPalette *palette_green;
    QPalette *palette_blue;
    QString encoded_filename;
    QString saved_filename;
    QString saved_short_filename;

signals:
    void saveRecordingComplete();
    void encodeRecordingComplete();
    void deleteRecordingComplete();

public slots:
    /* Start/stop button */
    void button_control_clicked(void);

    /* Reset button */
    void button_reset_clicked(void);

    /* Save button */
    void button_save_clicked(void);

    /* signal trigered encodeRecordingComplete */
    void startRecording(void);
    void stopRecording(void);
    void play(void);
    void stopPlaying(void);
    void encodeRecording(void);
    void deleteRecording(void);
    void saveRecording(void);

    /* menu actions */
    void setRecordingType_mp3(void);
    void setRecordingType_ogg(void);
    void setRecordingType_wma(void);
    void setRecordingType_wave(void);
    void setRecordingQuality44(void);
    void setRecordingQuality22(void);
    void setRecordingQuality11(void);
    void setRecordingQuality16(void);
    void setRecordingQuality8(void);
    void viewAbout(void);
    void viewStatus(void);

    void updateWindow();
    void finishedRecording(int exitCode, QProcess::ExitStatus exitStatus);
};

#endif // MAINWINDOW_H
